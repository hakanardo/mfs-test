import os
import time
from mfs import Mount, MFS, Retry


class Base(object):

    @classmethod
    def setup_class(cls):
        cls.mfs = MFS()
        cls.mnt = Mount(cls.mfs.master.ip)

    def mkdir(self, d):
        d = os.path.join(self.mnt.dir, d)
        if not os.path.exists(d):
            os.makedirs(d)

    def create(self, fn, content):
        self.mkdir(os.path.dirname(fn))
        with open(os.path.join(self.mnt.dir, fn), "w") as fd:
            fd.write(content)

    def unlink(self, fn):
        os.unlink(os.path.join(self.mnt.dir, fn))

    def verify(self, fn, content):
        with open(os.path.join(self.mnt.dir, fn), "r") as fd:
            assert fd.read() == content

    def count_chunks(self, path):
        return self.mnt.fileinfo(path).count("chunk ")

    def count_chunk_replicas(self, path):
        return self.mnt.fileinfo(path).count("copy ")

    def chunks(self, path):
        return self.mnt.chunks(path)

    def chunk_is_live(self, chunk):
        return chunk in self.mfs.live_chunks()

    def archives(self):
        return self.mfs.archives()

    def wait_while(self, condition, timeout=600):
        start = time.time()
        while (time.time() - start) < timeout:
            try:
                if not condition():
                    return
            except Retry:
                pass
            time.sleep(1)
        raise Exception("Timeout")

    def server_not_ok(self):
        try:
            self.mkdir("server_ok_test")
            n = len(self.mfs.chunk_servers)
            self.mnt.setgoal(n, "server_ok_test")
            self.create("server_ok_test/test", "Hi")
            ok = self.count_chunk_replicas("server_ok_test/test") != n
            self.unlink("server_ok_test/test")
        except (IOError, OSError):
            return True


class TestBasic(Base):

    def recreate(self):
        pass

    def test_create_file(self):
        self.create("a/b/tst1.txt", "Hello World!")
        self.create("a/b/tst2.txt", "Hi!")
        self.create("a/a/tst1.txt", "Ho!")
        self.recreate()
        self.verify("a/b/tst1.txt", "Hello World!")
        self.verify("a/b/tst2.txt", "Hi!")
        self.verify("a/a/tst1.txt", "Ho!")

    def test_replace_chunk_servers(self):
        assert len(self.mfs.chunk_servers) == 2
        self.mkdir("replicated")
        self.mnt.setgoal(2, "replicated")
        self.create("replicated/tst2.txt", "Hello!")
        assert self.count_chunks("replicated/tst2.txt") == 1
        assert self.count_chunk_replicas("replicated/tst2.txt") == 2

        self.mfs.kill_chunk_server()
        assert self.count_chunk_replicas("replicated/tst2.txt") == 1
        self.recreate()
        self.mfs.add_chunk_server()
        self.wait_while(lambda: self.count_chunk_replicas("replicated/tst2.txt") != 2)

        self.mfs.kill_chunk_server()
        self.mfs.add_chunk_server()
        assert self.count_chunk_replicas("replicated/tst2.txt") == 1
        self.verify("replicated/tst2.txt", "Hello!")

    def test_chunks_dissapears(self):
        self.create("dd/b/tst1.txt", "Hello World!")
        assert not self.mnt.settrashtime(1, "dd/b/tst1.txt")
        chunk, = self.chunks("dd/b/tst1.txt")
        assert self.chunk_is_live(chunk)
        self.recreate()        
        assert self.chunk_is_live(chunk)
        self.unlink("dd/b/tst1.txt")
        self.wait_while(lambda: self.chunk_is_live(chunk), 130*60)

class TestBasicRestart(TestBasic):
    def recreate(self):
        time.sleep(10)
        self.mfs.master.restart()
        self.wait_while(self.server_not_ok)

class TestBasicMetaLoggerRecreate(TestBasic):
    def recreate(self):
        time.sleep(10)
        self.mfs.reconstruct_master_from_logger()
        self.wait_while(self.server_not_ok)
