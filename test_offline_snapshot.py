from test_basic import Base
import time
import os
import py.test


class TestOfflineSnapshot(Base):

    def recreate(self):
        pass

    def test_basic(self):
        self.create("o1/b/tst1.txt", "Hello World!")
        assert not self.mnt.archive("o1", "backup.snap")
        self.recreate()
        assert not self.mnt.restore("backup.snap", "o2")
        self.verify("o1/b/tst1.txt", "Hello World!")
        self.verify("o2/b/tst1.txt", "Hello World!")
        assert 'o1' in self.mnt.list_archives()
        assert not self.mnt.archive("o2/b", "backup2.snap")
        assert 'o2/b' in self.mnt.list_archives()

    def test_archive_ids(self):
        self.create("ids1/b/tst1.txt", "Hello World 42!")
        aid = self.mnt.archive("ids1")
        assert self.mnt.list_archives().split('\n')[-2].split('|')[0].strip() == aid
        self.recreate()
        assert not self.mnt.restore_id(aid, "ids2")
        self.verify("ids1/b/tst1.txt", "Hello World 42!")
        self.verify("ids2/b/tst1.txt", "Hello World 42!")
        assert aid in self.mnt.list_archives()
        assert not self.mnt.unarchive_id(aid)
        assert aid not in self.mnt.list_archives()


    def test_unarchived_chunks_dissapear(self):
        self.mkdir("exp")
        assert not self.mnt.settrashtime(1, "exp")
        self.create("exp/a/tst1.txt", "Hello World!")
        self.create("exp/b/tst2.txt", "Ho Ho Ho!")
        chunka, = self.chunks("exp/a/tst1.txt")
        chunkb, = self.chunks("exp/b/tst2.txt")
        assert not self.mnt.archive("exp/a", "a.snap")
        assert not self.mnt.archive("exp/b", "b.snap")
        self.unlink("exp/a/tst1.txt")
        self.unlink("exp/b/tst2.txt")
        self.mnt.unarchive("a.snap")
        self.recreate()
        self.wait_while(lambda: self.chunk_is_live(chunka), 3 * 60 * 60)
        time.sleep(2)
        assert self.chunk_is_live(chunkb)
        assert not self.mnt.restore("b.snap", "exp/b2")
        self.verify("exp/b2/tst2.txt", "Ho Ho Ho!")
        assert self.mnt.restore("a.snap", "exp/a2")
        with py.test.raises(IOError):
            open(self.mnt.dir + "/exp/a2/tst1.txt").read()

    def test_unarchive_simple(self):
        self.create("ua/b/tst1.txt", "Hello World!")
        assert not self.mnt.settrashtime(1, "ua/b/tst1.txt")
        archives = len(self.archives())
        assert not self.mnt.archive("ua", "ua.snap")
        assert len(self.archives()) == archives + 1
        self.recreate()
        assert not self.mnt.unarchive("ua.snap")
        assert len(self.archives()) == archives
        assert self.mnt.restore("ua.snap", "ua2")
        assert not os.path.exists(self.mnt.dir + "/ua2/b/tst1.txt")

    def test_unarchive_related(self):
        self.mkdir("rel")
        assert not self.mnt.settrashtime(1, "rel")
        self.create("rel/a/tst1.txt", "Hello World!")
        self.create("rel/b/tst2.txt", "Ho Ho Ho!")
        chunka, = self.chunks("rel/a/tst1.txt")
        chunkb, = self.chunks("rel/b/tst2.txt")
        assert not self.mnt.archive("rel", "rel.snap")
        assert not self.mnt.archive("rel/a", "a2.snap")
        self.unlink("rel/a/tst1.txt")
        self.unlink("rel/b/tst2.txt")
        self.mnt.unarchive("rel.snap")
        self.recreate()
        self.wait_while(lambda: self.chunk_is_live(chunkb), 3 * 60 * 60)
        time.sleep(2)
        assert self.chunk_is_live(chunka)
        assert self.mnt.restore("rel.snap", "rel2")
        assert not self.mnt.restore("a2.snap", "rel/a2")
        self.verify("rel/a2/tst1.txt", "Hello World!")


class TestOfflineSnapshotRestart(TestOfflineSnapshot):

    def recreate(self):
        time.sleep(10)
        self.mfs.master.restart()
        self.wait_while(self.server_not_ok)


class TestOfflineSnapshotMetaLoggerRecreate(TestOfflineSnapshot):

    def recreate(self):
        time.sleep(10)
        self.mfs.reconstruct_master_from_logger()
        self.wait_while(self.server_not_ok)
