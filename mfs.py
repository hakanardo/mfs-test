from docker import Client
import tempfile
import time
import os
import io, re
import atexit
import subprocess
from threading import Thread
from StringIO import StringIO
import tarfile

docker = Client()
bindir = os.path.dirname(__file__) + "/client/usr/bin/"


class Host(object):
    image = 'mfs-test-runner'

    def __init__(self, *cmd):
        cmd = ' '.join(["'%s'" % a.replace("'", "\\'") for a in cmd])
        self.container = docker.create_container(self.image, cmd)
        docker.start(self.container, privileged=True)
        info = docker.inspect_container(self.container)
        self.ip = info['NetworkSettings']['IPAddress']
        atexit.register(self._cleanup)
        self.logger = Logger(self)
        self.logger.start()

    def _cleanup(self):
        if self.container:
            self.kill()

    def kill(self):
        docker.kill(self.container)
        docker.remove_container(self.container)
        self.container = None

    def logs(self, stream=False):
        return docker.logs(self.container, stream=stream)

    def has_file(self, fn):
        return len(docker.copy(self.container, fn).data) > 0

    def restart(self):
        docker.kill(self.container, "INT")
        docker.wait(self.container)
        self.logger.join()
        docker.start(self.container, privileged=True)
        self.logger = Logger(self)
        self.logger.start()


class ReconstructedHost(Host):
    image = 'mfs-test-runner-reconstructed'


class Logger(Thread):
    daemon = True

    def __init__(self, host):
        Thread.__init__(self)
        self.host = host

    def run(self):
        for l in self.host.logs(True):
            # print '%s: %s' % (self.host.ip, l),
            print l,


class MFS(object):

    def __init__(self, chunk_servers=2, loggers=1):
        # Silly hack to make the master keep it's IP when restored
        self.master_front = Host('bash', '-c', 'ifconfig eth0 down; sleep inf')
        self.master_ip = self.master_front.ip
        print 'Master IP: ', self.master_ip
        self.master = Host('/etc/mfs/start_master.sh',
                           self.master_ip,
                           'mfsmaster', '-d')
        self.chunk_servers = []
        self.loggers = []
        for i in xrange(chunk_servers):
            self.add_chunk_server()
        for i in xrange(loggers):
            self.add_logger()

    def add_chunk_server(self):
        self.chunk_servers.append(Host('/etc/mfs/start.sh',
                                       self.master_ip,
                                       'mfschunkserver', '-d'))

    def kill_chunk_server(self):
        self.chunk_servers[0].kill()
        self.chunk_servers = self.chunk_servers[1:]

    def add_logger(self):
        self.loggers.append(Host('/etc/mfs/start.sh',
                                 self.master_ip,
                                 'mfsmetalogger', '-d'))

    def reconstruct_master_from_logger(self):
        logger = self.loggers[0]
        data = docker.copy(logger.container,
                           "/var/lib/mfs").data
        fobj = io.BytesIO(data)
        tar = tarfile.open(fileobj=fobj, mode="a")
        docker_file = "\n".join(["FROM mfs-test-runner",
                                 "RUN rm -r /var/lib/mfs",
                                 "ADD mfs /var/lib/mfs",
                                 "RUN mfsmetarestore -a"])
        ti = tarfile.TarInfo("Dockerfile")
        ti.size = len(docker_file)
        tar.addfile(ti, StringIO(docker_file))
        tar.close()

        fobj.flush()
        fobj.seek(0)
        for s in docker.build(fileobj=fobj, custom_context=True,
                              tag="mfs-test-runner-reconstructed"):
            result = eval(s)
            if hasattr(result, "error"):
                print result["error"]
                assert False
            print result.get("stream", ""),

        self.master.kill()
        self.master = ReconstructedHost('/etc/mfs/start_master.sh',
                                        self.master_ip,
                                        'mfsmaster', '-d')

    def live_chunks(self, versions=True):
        chunks = []
        for server in self.chunk_servers:
            for disk in ["/mnt/hd1", "/mnt/hd2"]:
                data = docker.copy(server.container, disk).data
                tar = tarfile.open(fileobj=StringIO(data))
                chunks.extend(os.path.basename(f.name)
                              for f in tar.getmembers() if not f.isdir())
        if versions:
            return set([c[6:-4] for c in chunks if c != '.lock'])
        else:
            return set([c[6:-13] for c in chunks if c != '.lock'])

    def archives(self):
        data = docker.copy(self.master.container,
                           "/var/lib/mfs/archives").data
        tar = tarfile.open(fileobj=StringIO(data))
        return [f.name for f in tar.getmembers() if not f.isdir()]

class Retry(Exception):
    pass
    
class Mount(object):

        def __init__(self, ip):
            self.dir = tempfile.mkdtemp()
            atexit.register(self._cleanup)
            for i in xrange(10):
                if subprocess.call([bindir + "mfsmount", "-H", ip, self.dir]):
                    time.sleep(1.2 ** i)
                else:
                    break
            else:
                raise Exception("Failed to mfsmount.")

        def _cleanup(self):
            while subprocess.call(["fusermount", "-u", self.dir]):
                time.sleep(1)
            os.rmdir(self.dir)

        def setgoal(self, goal, path):
            path = os.path.join(self.dir, path)
            return subprocess.call([bindir + "mfssetgoal", str(goal), path])

        def settrashtime(self, ttime, path):
            path = os.path.join(self.dir, path)
            return subprocess.call([bindir + "mfssettrashtime", str(ttime),
                                    path])

        def fileinfo(self, path):
            path = os.path.join(self.dir, path)
            try:
                return subprocess.check_output([bindir + "mfsfileinfo",  path])
            except subprocess.CalledProcessError as e:
                print e
                raise Retry

        def chunks(self, path):
            chunks = []
            for line in self.fileinfo(path).split('\n'):
                match = re.search(r'chunk \d+: ([^\s]+)', line)
                if match:
                    chunks.append(match.group(1))
            return chunks

        def archive(self, src, dst=None):
            src = os.path.join(self.dir, src)
            if dst is None:
                return subprocess.check_output([bindir + "mfsarchive", src]).strip()
            else:
                dst = os.path.join(self.dir, dst)
                return subprocess.call([bindir + "mfsarchive", src, dst])

        def unarchive(self, src):
            src = os.path.join(self.dir, src)
            return subprocess.call([bindir + "mfsunarchive", src, self.dir])
        
        def unarchive_id(self, src):
            return subprocess.call([bindir + "mfsunarchive", "-i", src, self.dir])

        def restore(self, src, dst):
            src = os.path.join(self.dir, src)
            dst = os.path.join(self.dir, dst)
            return subprocess.call([bindir + "mfsrestore", src, dst])

        def restore_id(self, src, dst):
            dst = os.path.join(self.dir, dst)
            return subprocess.call([bindir + "mfsrestore", "-i", src, dst])

        def list_archives(self):
            return subprocess.check_output([bindir + "mfslistarchives", self.dir])
