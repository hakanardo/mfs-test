test:
	py.test -v

update:
	-rm ../mfs*.deb
	cd ../mfs-1.6.27 && fakeroot debian/rules binary
	-mkdir context/debs
	cp ../mfs*.deb context/debs/
	dpkg -x ../mfs-client_*.deb  client
	$(MAKE) build

build:
	docker.io build -t mfs-test-runner context 

test_%:
	py.test -v -s $@.py
